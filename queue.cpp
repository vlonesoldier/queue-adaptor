#include <string>
#include <iostream>

using namespace std;

struct element
{
    string songName;
    element *next;
};

void enqueue(element **last, element **first)
{
    element *item = new element;
    getline(std::cin >> std::ws, item->songName);
    
    if(*first == NULL) // assign new structure to first and last when queue empty
        *first = *last = item;
    
    else { // if not empty assign newly created item to last's property next; then change *last address to item address
        (*last)->next = item;
        *last = item;
    }
}

void dequeue(element **last, element **first)
{
    if (*first)
    {       
        
        element *item = *first;
        cout << "Removed song from playlist: " << item->songName << endl;
        
        *first = (*first)->next;
        delete item;

    }
    else
        cout << "Nothing to remove" << endl;

}

void list(element **first) {

    element *firstTolist = *first;
    while(firstTolist) {
        
        cout << "Name of the song: " << firstTolist->songName << endl;
        firstTolist = firstTolist->next;
    }
}

int main()
{

    int option;
    element *last = NULL;
    element *first = NULL;

    printf("Choose an option\n"
           "Pick a number:\n"
           "1 - Add song to playlist\n"
           "2 - Remove song from playlist\n"
           "3 - List all songs from playlist\n"
           "0 - Finish program\n\n");

    while (scanf("%d", &option))
    {
        switch (option)
        {
        case 1:
            enqueue(&last, &first); // adres last
            break;

        case 2:
            dequeue(&last, &first);
            break;

        case 3:
            list(&first);
            break;

        case 0:
            return 0;
        
        default:
            cout << "Try choose right option" << endl;
            break;
        }
    }

    return 0;
}