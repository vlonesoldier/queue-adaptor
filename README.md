# Queue adaptor
### Program enables you providing best songs to your playlist!

**This is how the main menu looks like whilst starting the program:**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Choose an option
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pick a number:
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 - Add song to playlist
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2 - Remove song from playlist
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3 - List all songs from playlist
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0 - Finish program

#### To compile a program just type:
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; g++ -o queue queue.cpp
